class CreateElections < ActiveRecord::Migration
  def change
    create_table :elections do |t|
      t.string :name
      t.integer :election_type
      t.integer :year

      t.timestamps
    end
  end
end
