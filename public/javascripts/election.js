var election = angular.module('election', ['rest_store']);

function ElectionCtrl($scope, Election) {

    $scope.totalTodos=4;

//    $scope.elections = [{name: "NPE-2014", election_type:1, year: 2014, done:false}, {name: "LGE-2016", election_type:2, year: 2016, done:false}];

    $scope.elections = Election.query();

    $scope.addTodo = function() {

        $scope.elections.push({name: $scope.formElectionName, election_type:$scope.formElectionType, year: $scope.formElectionYear, done: false});
        $scope.formElectionName = "";
        $scope.formElectionType = "";
        $scope.formElectionYear = "";

    }

    $scope.getTotalElections = function() {


        return $scope.elections.length;
    }

    $scope.clearCompleted = function() {

        $scope.elections = _.filter($scope.elections, function(election) {
            return !election.done;
        });

    }
};
