var selectListApp = angular.module('selectListApp',[]);

function SelectListCtrl($scope) {
    $scope.setUpcoming = function(b) {
        $scope.wantsUpcomingElection = b;
        $scope.wantsListRevision = !b;
    };
    $scope.updateRegionalityControls = function() {
        if ($scope.selectedUpcomingElection && $scope.selectedElectionType) {
            $scope.upcomingRegionality = true;
        } else {
            $scope.upcomingRegionality = false;
        }
    };
    $scope.upcomingElections = [
        { id: 1, name: "NPE 2014" },
        { id: 2, name: "LGE 2016" }
    ];
    $scope.electionTypes = [
        { id: 1, name: "Metro" },
        { id: 1, name: "Local" },
        { id: 1, name: "District" }
    ];
}
