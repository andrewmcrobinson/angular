angular.module('rest_store', ['ngResource']).
  factory('Election', function($resource) {
    var Election = $resource('elections/:id');
    return Election;
  });
